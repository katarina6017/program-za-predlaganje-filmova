# Movie Recommendation Program


The Movie Recommendation Program is an application that uses machine learning techniques to suggest movies based on your personal preferences. You can input your favorite genres, directors, or actors, and the application will recommend movies that match your interests.

This project was developed as a course assignment for the 'Applied Machine Learning' course.


## How It's Made:

**Tech used:** Python 3.x, Flask, Pandas, TMDb API




## Usage

Once you have the application running, you can use a simple web interface to enter your movie preferences. The application will then suggest movies based on the information you provided.


## Documentation

[Documentation](https://gitlab.com/katarina6017/program-za-predlaganje-filmova/-/blob/main/PSU_dokumentacija.docx?ref_type=heads)

